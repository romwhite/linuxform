---
title: About me
subtitle: Why you'd want to hang out with me
comments: false
draft: false
---

*Имя:* Роман

*Интересы:* FOSS, Си, Лисп, устройство операционных систем. 

*Любимые программы:* Emacs, Neovim

*Любимый фильм:* Джеки Браун (смотреть строго в ориг. озвучке)

*DE(WM):* Gnome, i3, Sway

*Обратная связь:* Mastodon @romwhite@mastodon.social

Отдельный интерес - философия. Несколько лет усиленно изучал философию, планировал писать кандидатскую диссертацию на тему "Философия Мераба Мамардашвили". Очень уважаю Александра Пятигорского, особенно работы, посвященные философии буддизма. В последнее время читаю больше о постмодернизме (и пост- пост- модернизме)).

## Bio

Всё идёт из детства. Моё прошло на рубеже 80-х и 90-х и запомнилось мне тем, что в начале было невообразимо скучно. По двум телевизионным программам показывали съезды, "Прожектор Перестройки", программу "Время" и совершенно неинтересные и некрасивые фильмы. Отчасти выручали книги. Но их набор в библиотеке был либо органичен, либо они были слишком сложными для моего возраста. Гулять и общаться со сверстниками мне не особо нравилось - в большинстве своём они казались мне если не глупыми, то как будто ограниченными. Помню как дни проходили  в маяте, когда я цеплялся ко взрослым и надоедал им. Или в выполнении каких-то глупых обязанностей, которые ненавидел (типа хождения в школу).

Всё изменилось с появлением дома в 1993-м первого компьютера - клона ZX-Spectrum. После этого жизнь стала другой. Появилось новое измерение. С компьютером никогда не скучно. Ты можешь пытаться постигнуть его суть, но пределы  постижения отсутствуют. 

С конца 90-х я экспериментировал с Linux: RedHat 5.2, Mandrake, Slackware. У меня было ощущение, что за этим скрывается другой мир. Постепенно компьютеры в коллективном представлении стали чем-то обыденным, вроде микроволновки - вещь в себе, выполняющая требуемую функцию. Мне не хотелось с этим мириться и я заново открыл для себя мир FOSS.

Пришёл к выводу, что FOSS - это величайшее достижение человеческой цивилизации на её современном этапе. Мы можем восторгаться величием египетских пирамид, но у нас под носом есть другое достижение, в котором участвуют тысячи, если не миллионы людей. Они вкладывают свои знания и умения, движимые побуждением к творчеству, и создают программы, доступные всем желающим абсолютно бесплатно. Написаны миллионы строк кода, воплощающих красивые концепции и идеи. И всё это доступно для использования и изучения. Всё, что ограничивает, это собственные знания и интеллектуальные возможности.

Этот сайт - форма диалога с сами собой. В моём окружении нет людей, со схожими интересами. А поговорить хочется. Пытаюсь писать не имея в голове внутреннего цензора и не ограничиваясь воображаемой аудиторией.

Большую часть информации получаю на английском, поэтому ссылки на англоязычные ресурсы.
